﻿using System.Collections;

namespace CleanCoder
{
    public static class Algorithms
    {
        public static long CalculateFactorial(uint number)
        {
            long result = 1;
            for (uint i = 2; i <= number; i++)
            {
                result *= i;
            }
            return result;
        }

        /// <summary>
        /// Sieve(list) is filled with all prime numbers upto given target
        /// </summary>
        /// <param name="target">max number to which search primes</param>
        /// <returns></returns>
        /// Optimizations:
        /// - initialize list with max possible prime numbers using Legendre's constant
        /// - omit even numbers in the loops
        /// - start crossing numbers from power of number and jump to next number with multiply by 2
        /// - use BitArray instead of byte array(memory optimization)
        /// - loop upto square of the target
        /// source for optimizations: http://digitalbush.com/2010/02/26/sieve-of-eratosthenes-in-csharp/
        /// https://web.archive.org/web/20101121215829/https://digitalbush.com/2010/02/26/sieve-of-eratosthenes-in-csharp/
        public static IList<int> SieveOfEratosthenes(int target)
        {
            var primes = new List<int>((int)(target / (Math.Log(target) - 1.08366)));   //Legendre's constant
            var eliminated = new BitArray(target + 1);
            var maxSquareRoot = Math.Sqrt(target);

            primes.Add(2);

            for (var candidate = 3; candidate <= target; candidate += 2)
            {
                if (eliminated[candidate]) continue;
                if (candidate<maxSquareRoot)
                {
                    for (var multiple = candidate * candidate; multiple <= target; multiple += 2 * candidate)
                    {
                        eliminated[multiple] = true;
                    }
                }
                primes.Add(candidate);
            }

            return primes;
        }

        /// <summary>
        /// Sieve(list) is filled with all prime numbers in the range fromNumber - upToNumber
        /// </summary>
        /// <param name="fromNumber">Number from which calculations starts</param>
        /// <param name="upToNumber">Number to which calculations ends</param>
        /// <returns></returns>
        public static IList<int> SieveOfEratosthenes(int fromNumber, int upToNumber)
        {
            return SieveOfEratosthenes(upToNumber).Where(e => e > fromNumber).ToList();
        }

        /// <summary>
        /// Returns all prime factors of a given number
        /// </summary>
        /// <param name="number">Find all prime factors for specific number</param>
        /// <returns></returns>
        public static IList<int> AllPrimeFactorsOfAGivenNumber(int number)
        {
            var primeFactors = new List<int>();

            var primes = SieveOfEratosthenes(number);
            foreach (var prime in primes)
            {
                while (number%prime == 0)
                {
                    number /= prime;
                    primeFactors.Add(prime);
                }
            }

            return primeFactors;
        }
    }
}
