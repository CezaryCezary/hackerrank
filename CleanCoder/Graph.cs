﻿namespace CleanCoder
{
    public class Node
    {
        public Node()
        {
            Nodes = new List<int>();
            Visited = false;
        }

        public void AddNode(int node)
        {
            Nodes.Add(node);
        }
        public List<int> Nodes { get; set; }
        public bool Visited { get; set; }
    }

    public class Graph
    {
        public List<int> BreadthFirstSearch(Node[] graph)
        {
            var queue = new Queue<int>();
            var result = new List<int>(graph.Length);

            //setup visited to false each time of run

            queue.Enqueue(0);//enqueue first element from not checked graph, first NotVisited Node
            graph[0].Visited = true;

            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                result.Add(node);

                foreach (var connectedNode in graph[node].Nodes)
                {
                    if (!graph[connectedNode].Visited)
                    {
                        queue.Enqueue(connectedNode);
                        graph[connectedNode].Visited = true;
                    }
                }
            }

            return result;
        }
    }
}
