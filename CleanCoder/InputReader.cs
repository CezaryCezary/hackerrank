namespace CleanCoder
{
    public static class InputReader
    {
        public static int ReadInt()
        {
            return int.Parse(Console.ReadLine());
        }

        public static int[] ReadIntArray()
        {
            return Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
        }
    }
}