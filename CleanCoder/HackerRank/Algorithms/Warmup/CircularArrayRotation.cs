﻿namespace CleanCoder.HackerRank.Algorithms.Warmup
{
    /// <summary>
    /// Circular Array Rotation, make right rotation by simple adding
    /// desired shift at addition moment, no need to shift at all
    /// https://www.hackerrank.com/challenges/circular-array-rotation
    /// </summary>
    public class CircularArrayRotation
    {
        public static void Solve()
        {
            var str = Console.ReadLine().Split(' ');

            var n = Convert.ToInt32(str[0]);
            var k = Convert.ToInt32(str[1]);
            var q = Convert.ToInt32(str[2]);

            var array = new int[n];

            str = Console.ReadLine().Split(' ');

            for (var i = 0; i < n; i++)
            {
                array[(i + k) % n] = Convert.ToInt32(str[i]);
            }

            while (q-- > 0)
            {
                var index = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine(array[index]);
            }
        }
    }
}
