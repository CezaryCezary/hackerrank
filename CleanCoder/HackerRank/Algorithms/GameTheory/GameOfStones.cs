﻿namespace CleanCoder.HackerRank.Algorithms.GameTheory
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/game-of-stones-1
    /// </summary>
    public class GameOfStones
    {
        /// <summary>
        /// Solution with building Solution Matrix based on initial fields
        /// </summary>
        public static void Solve()
        {
            var t = int.Parse(Console.ReadLine());
            var solutionMatrix = BuildSolutionMatrix(101);
            while (t-- > 0)
            {
                var n = int.Parse(Console.ReadLine());
                Console.WriteLine(solutionMatrix[n] ? "First" : "Second");
            }
        }

        static bool[] BuildSolutionMatrix(int n)
        {
            var arr = new bool[n + 1];

            for (int i = 2; i <= n; ++i)
            {
                if (i >= 2 && i <= 5) arr[i] = true;
                else if (arr[i - 2] == false || arr[i - 3] == false || arr[i - 5] == false) arr[i] = true;
            }

            return arr;
        }

        /// <summary>
        /// Solution based on observation on solution matrix that only for n % 7 second player can win
        /// </summary>
        public static void SolveOtherWay()
        {
            var t = int.Parse(Console.ReadLine());
            while (t-- > 0)
            {
                var n = int.Parse(Console.ReadLine());
                Console.WriteLine(n % 7 > 1 ? "First" : "Second");
            }
        }
    }
}
