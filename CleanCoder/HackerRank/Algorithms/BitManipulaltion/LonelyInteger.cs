﻿using System.Collections;

namespace CleanCoder.HackerRank.Algorithms.BitManipulaltion
{
    /// <summary>
    /// Find unique element, rest is in pairs.
    /// Use BitArray to save if element exists or not.
    /// While reading the elements change the sign of the index.
    /// Search for the index, which is changed once, so is true, not false as other.
    /// https://www.hackerrank.com/challenges/lonely-integer
    /// </summary>
    public static class LonelyInteger
    {
        public static void Solve()
        {
            Console.ReadLine();
            var array = new BitArray(101);

            var elems = Console.ReadLine().Split(' ');
            if (elems.Length == 1)
            {
                Console.WriteLine(Convert.ToInt32(elems[0]));
                return;
            }

            for (var i = 0; i < elems.Length; i++)
            {
                var elem = Convert.ToInt32(elems[i]);
                array[elem] = !array[elem];
            }

            for (var i = 0; i < array.Length; i++)
            {
                if (array[i])
                {
                    Console.WriteLine(i);
                    break;
                }
            }
        }
    }
}
