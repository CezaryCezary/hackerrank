﻿namespace CleanCoder.HackerRank.Algorithms.Implementation
{
    /// <summary>
    /// 6 of 14 is passed, other WA or TLE.
    /// https://www.hackerrank.com/challenges/bonetrousle
    /// Optimizations: The sum of the first n natural numbers - n(n+1)/2
    /// Solution: https://github.com/ynyeh0221/Hackerrank/blob/master/Bonetrousle.py
    /// </summary>
    public class Bonetrousle
    {
        public static void ReadInput()
        {
            var t = Convert.ToInt32(Console.ReadLine());

            while (t-- > 0)
            {
                var str = Console.ReadLine().Split(' ');
                var n = Convert.ToUInt64(str[0]);
                var k = Convert.ToUInt64(str[1]);
                var b = Convert.ToUInt32(str[2]);

                var result = Solve(n, k, b);

                if (result.Count == 0)
                {
                    Console.WriteLine(-1);
                }
                else
                {
                    for (var i = 0; i < result.Count - 1; ++i)
                    {
                        Console.Write(result[i]);
                        Console.Write(' ');
                    }
                    Console.Write(result[result.Count - 1]);
                    Console.Write('\n');    
                }
            }
        }

        public static List<ulong> Solve(ulong n, ulong k, uint b)
        {
            ulong maxSum = k%2 == 0 ? k / 2 * (k + 1) : (k+1) / 2 * k;

            if (n > maxSum || n < b*(b+1)/2)
            {
                return new List<ulong>();
            }

            var result = new List<ulong>((int)b);

            var value = n/b;
            var halfValue = b/2;
            value = value - halfValue < 1 ? 1 : value - halfValue;

            ulong sum = 0;
            for(var i=0; i<b; ++i)
            {
                result.Add(value + (ulong) i);
                sum += result[i];
            }

            if (sum == n)
            {
                return result;
            }

            var diff = n - sum;
            var counter = result.Count - 1;
            while (diff > 0)
            {
                result[counter--]++;
                if (result[counter+1] > k)
                {
                    return new List<ulong>();
                }
                diff--;
                if (counter == 0)
                {
                    counter = result.Count - 1;
                }
            }

            return result;
        }
    }
}
