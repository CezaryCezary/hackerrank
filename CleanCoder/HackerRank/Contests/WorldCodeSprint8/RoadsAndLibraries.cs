﻿namespace CleanCoder.HackerRank.Contests.WorldCodeSprint8
{
    /// <summary>
    /// https://www.hackerrank.com/contests/world-codesprint-8/challenges/torque-and-development
    /// 
    /// based on http://ideone.com/KUJqFF
    /// </summary>
    public class RoadsAndLibraries
    {
        public class Node
        {
            public Node()
            {
                Cities = new List<int>();
            }
            public List<int> Cities { get; set; }
            public bool Visited { get; set; }
        }

        public static void Solve()
        {
            int q = int.Parse(Console.ReadLine());
            while (q-- > 0)
            {
                string[] tokens_n = Console.ReadLine().Split(' ');
                int n = Convert.ToInt32(tokens_n[0]);
                int m = Convert.ToInt32(tokens_n[1]);
                int libCost = Convert.ToInt32(tokens_n[2]);
                int roadCost = Convert.ToInt32(tokens_n[3]);

                Console.WriteLine(Count(n, m, libCost, roadCost));
            }
        }

        private static long Count(int n, int m, int libCost, int roadCost)
        {
            var graph = new Node[n + 1];

            for (int i = 0; i < n + 1; i++)
            {
                graph[i] = new Node();
            }

            for (int i = 0; i < m; i++)
            {
                string[] tokens_city = Console.ReadLine().Split(' ');
                int city1 = Convert.ToInt32(tokens_city[0]);
                int city2 = Convert.ToInt32(tokens_city[1]);
                graph[city1].Cities.Add(city2);
                graph[city2].Cities.Add(city1);
            }

            long cost = 0;
            for (int i = 1; i <= n; i++)
            {
                if (!graph[i].Visited)
                {
                    var nodes = DepthFirstSearch(graph, i);
                    cost += libCost;
                    if (libCost > roadCost)
                    {
                        cost += roadCost*(nodes - 1);
                    }
                    else
                    {
                        cost += libCost*(nodes - 1);
                    }
                }
            }

            return cost;
        }

        private static long DepthFirstSearch(Node[] graph, int startingNode)
        {
            var stack = new Stack<int>();
            stack.Push(startingNode);

            long nodes = 0;

            while (stack.Count != 0)
            {
                var v = stack.Pop();
                if (!graph[v].Visited)
                {
                    ++nodes;
                    graph[v].Visited = true;
                    foreach (var city in graph[v].Cities)
                    {
                        stack.Push(city);
                    }
                }
            }

            return nodes;
        }
    }
}
