﻿namespace CleanCoder.HackerRank.Contests.WeekOfCode28
{
    /// <summary>
    /// https://www.hackerrank.com/contests/w28/challenges/value-of-friendship
    /// 
    /// Hints - https://www.hackerrank.com/contests/w28/challenges/value-of-friendship/forum
    /// 1. Count additional edges at the end of all edges connecting the graph
    /// Opitimization:
    /// 1. Precalculate all maximum points for given number of nodes
    /// </summary>
    public class TheValueOfFriendship
    {
        public class Node
        {
            public Node()
            {
                Nodes = new List<int>();
                Visited = false;
            }

            public void AddNode(int node)
            {
                Nodes.Add(node);
            }

            public List<int> Nodes { get; }
            public bool Visited { get; set; }
        }

        public static void Solve()
        {
            int t = Convert.ToInt32(Console.ReadLine());
            while (t-- > 0)
            {
                string[] tokens_n = Console.ReadLine().Split(' ');
                int n = Convert.ToInt32(tokens_n[0]);
                int m = Convert.ToInt32(tokens_n[1]);

                //initialize Graph
                var graph = new Node[n];
                for (int i = 0; i < n; i++)
                {
                    graph[i] = new Node();
                }

                //build graph's adjacency list
                for (int i = 0; i < m; i++)
                {
                    string[] x = Console.ReadLine().Split(' ');
                    int a = Convert.ToInt32(x[0]) - 1;
                    int b = Convert.ToInt32(x[1]) - 1;
                    graph[a].AddNode(b);
                    graph[b].AddNode(a);
                    //Console.WriteLine($"a:{a}:b:{b}");
                }

                Console.WriteLine((long) Count(graph));
            }
        }

        public static long Count(Node[] graph)
        {
            var startingNode = 0;
            var subGraphs = new List<SubGraph>();

            while (startingNode != -1)
            {
                //make BFS to count nodes and edges on each subgraph
                var subGraph = BfsWithCountingNodes(graph, startingNode);

                if (subGraph.Edges > 0) subGraphs.Add(subGraph);

                //search for next subgraph
                //Console.WriteLine("startingNode:" + startingNode);
                while (graph[startingNode].Visited)
                {
                    if (startingNode == graph.Length - 1)
                    {
                        startingNode = -1;
                        break;
                    }
                    ++startingNode;
                }
            }

            subGraphs.Sort((x, y) => -1 * x.Max.CompareTo(y.Max));

            var pointResults = PrecalculatePoints(subGraphs[0].Nodes);

            //Add missing points by subgraphs correlation
            long total = pointResults[subGraphs[0].Nodes];
            long sumMax = subGraphs[0].Max;
            long remainingEdges = subGraphs[0].RemainingEdges;
            for (int i = 1; i < subGraphs.Count; ++i)
            {
                total += pointResults[subGraphs[i].Nodes] + (subGraphs[i].Edges - subGraphs[i].RemainingEdges) * sumMax;
                sumMax += subGraphs[i].Max;
                remainingEdges += subGraphs[i].RemainingEdges;
            }

            return total + remainingEdges * sumMax;
        }

        private static long[] PrecalculatePoints(int maxNodes)
        {
            var pointResults = new long[maxNodes + 1];

            long points = 0;
            for (int i = 2; i <= maxNodes; ++i)
            {
                points += (long)i * (i - 1);
                pointResults[i] = points;
            }

            return pointResults;
        }

        private static SubGraph BfsWithCountingNodes(Node[] graph, int startingNode)
        {
            var queue = new Queue<int>();
            var nodesCounter = 0;
            var edgesCounter = 0;

            queue.Enqueue(startingNode);
            graph[startingNode].Visited = true;

            while (queue.Count > 0)
            {
                var node = queue.Dequeue();
                ++nodesCounter;

                foreach (var connectedNode in graph[node].Nodes)
                {
                    ++edgesCounter;
                    if (!graph[connectedNode].Visited)
                    {
                        queue.Enqueue(connectedNode);
                        graph[connectedNode].Visited = true;
                    }
                }
            }

            return new SubGraph { Nodes = nodesCounter, Edges = edgesCounter / 2 };
        }

        private class SubGraph
        {
            public int Nodes { get; set; }
            public int Edges { get; set; }
            public long Max => (long)Nodes * (Nodes - 1);
            public int RemainingEdges => Edges - Nodes + 1;
        }
    }
}
