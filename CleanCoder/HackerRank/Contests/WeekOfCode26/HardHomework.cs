﻿namespace CleanCoder.HackerRank.Contests.WeekOfCode26
{
    /* Clean and easy solution:
     * 	int i, n;
    double t, ans;

    scanf("%d", &n);
    vector<double> u(n+1), d(n+1), m(n+1);
    u[0]=u[1]=-10.0;
    d[0]=d[1]=+10.0;
    for(i=2; i<n; i++) {
        t=cos(i/2.0-1);
    u[i]=max(u[i - 2], t);
    d[i]=min(d[i - 2], t);
    t=2* sin(i/2.0);
    m[i]=max(t* u[i], t* d[i]);
}
ans=-10;
    for(i=2; i<=n-1; i++) {
        t=m[i]+sin(n-i);
ans=max(ans, t);
    }

    printf("%.9lf\n", ans);
    return 0;
}*/
    /// <summary>
    /// This simple solution gives 49.14 out of 75 points
    /// https://www.hackerrank.com/contests/w26/challenges/hard-homework
    /// </summary>
    public class HardHomework
    {
        public HardHomework()
        {
            int n = InputReader.ReadInt();
            Console.WriteLine($"{Solve(n):F9}");
        }

        public double Solve(int n)
        {
            var arr = new double[n - 1];
            for (int i = 1; i < n - 1; ++i)
            {
                arr[i] = Math.Sin(i);
            }

            double result = double.MinValue;
            int top = n / 3 + 1;
            for (int i = 1; i < top; ++i)
            {
                var j = i;
                var k = n - i - j;
                while (j <= k)
                {
                    result = Math.Max(arr[i] + arr[j] + arr[k], result);
                    ++j; --k;
                }
            }

            return result;
        }
    }
}
