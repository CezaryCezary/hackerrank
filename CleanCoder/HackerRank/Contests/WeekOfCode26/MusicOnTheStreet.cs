﻿namespace CleanCoder.HackerRank.Contests.WeekOfCode26
{
    /// <summary>
    /// https://www.hackerrank.com/contests/w26/challenges/street-parade-1
    /// </summary>
    public class MusicOnTheStreet
    {
        static readonly int[] Arr = new int[1000003];

        /// <summary>
        /// Solution based on nice and simple solution of one of the competitors
        /// </summary>
        public static void Solve()
        {
            //ReadInput
            int n = InputReader.ReadInt();
            var inputArray = Console.ReadLine().Split(' ');

            for (int i = 1; i <= n; ++i)
            {
                Arr[i] = int.Parse(inputArray[i - 1]);
            }

            var temp = InputReader.ReadIntArray();
            var m = temp[0];
            int min = temp [1];
            int max = temp[2];

            //Solve
            Arr[0] = Arr[1] - max - 1;
            Arr[n + 1] = Arr[n] + max + 1;
            int breakPoint = 1;

            for (int i = 1; i <= n; ++i)
            {
                if (Arr[i + 1] - Arr[i] < min)
                {
                    if (Arr[i] - Arr[breakPoint] + max >= m)
                    {
                        Console.WriteLine(Arr[breakPoint] - max);
                        return;
                    }
                    breakPoint = i + 1;
                }
                else if (Arr[i+1] - Arr[i] > max)
                {
                    int ok = 0;
                    if (Arr[breakPoint] - Arr[breakPoint - 1] >= min)
                    {
                        ok = Math.Min(Arr[breakPoint] - Arr[breakPoint - 1], max);
                    }
                    if (Arr[i] + max - Arr[breakPoint] + ok >= m)
                    {
                        Console.WriteLine(Arr[breakPoint] - ok);
                        return;
                    }
                    breakPoint = i + 1;
                }
            }
        }
        
        /// <summary>
        /// Solution based on trial and error method.
        /// </summary>
        public static void ReadConsoleInput()
        {
            int n = InputReader.ReadInt();
            var inputArray = InputReader.ReadIntArray();

            var temp = InputReader.ReadIntArray();
            Console.WriteLine(SolveTrialAndErrorMethod(n, inputArray, temp[0], temp[1], temp[2]));
        }

        public static int SolveTrialAndErrorMethod(int n, int[] arr, int m, int min, int max)
        {
            if (m <= max && m >= min)
            {
                return arr[0] - m;
            }

            int breakPoint = 0;
            int sum = max;
            int result = -1;
            int i = 1;
            for (; i < n; ++i)
            {
                int diff = arr[i] - arr[i - 1];
                if (diff > max)
                {
                    sum += max;
                    if (sum >= m && sum - 2 * (max - min) <= m)
                    {
                        result = arr[i - 1] - m + max;
                        break;
                    }
                    sum = Math.Min(diff, max);
                    breakPoint = i;
                    continue;
                }
                if (diff < min)
                {
                    sum = 0;
                    breakPoint = i;
                    continue;
                }
                sum += diff;

                if (sum >= m)
                {
                    result = arr[i] - m;
                    if (Math.Abs(m - arr[i]) < min) result = arr[breakPoint] - min;
                    break;
                }
            }

            if (i == n)
            {
                sum += max;
                if (sum == m)
                {
                    result = arr[n - 1] - sum + max;
                }
                else
                {
                    result = arr[breakPoint] - Math.Min(Math.Max(m - sum, min), max);
                }
            }

            return result;
        }
    }
}
