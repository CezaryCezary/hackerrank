﻿using System.Numerics;

namespace CleanCoder.HackerRank.Contests.WorldCodeSprint7
{
    class Interval : IComparable<Interval>
    {
        public int Row { get; set; }
        public int Begin { get; set; }
        public int End { get; set; }

        public int CompareTo(Interval other)
        {
            return Begin > other.Begin ? 1 : End > other.End ? 1 : 0;
        }
    }

    /// <summary>
    /// https://www.hackerrank.com/contests/world-codesprint-7/challenges/gridland-metro
    /// https://www.hackerrank.com/challenges/gridland-metro
    /// </summary>
    public class GridlandMetro
    {
        public void Solve()
        {
            var c = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
            int n = c[0];
            int m = c[1];
            int k = c[2];

            var list = new List<Interval>(k);

            for (int i = 1; i <= k; ++i)
            {
                c = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);
                var all = list.FindAll(e => e.Row == c[0]);
                if (all.Count == 0)
                {
                    list.Add(new Interval
                    {
                        Row = c[0],
                        Begin = c[1],
                        End = c[2]
                    });
                }
                else
                {
                    int begin = c[1];
                    int end = c[2];
                    bool toAdd = true;
                    foreach (var interval in all)
                    {
                        if (begin >= interval.Begin && begin <= interval.End + 1)
                        {
                            interval.End = Math.Max(interval.End, end);
                            toAdd = false;
                        }
                        if (end >= interval.Begin + 1 && end <= interval.End)
                        {
                            interval.Begin = Math.Min(interval.Begin, begin);
                            toAdd = false;
                        }
                        if (begin < interval.Begin && end > interval.End)
                        {
                            interval.Begin = begin;
                            interval.End = end;
                            toAdd = false;
                        }
                    }
                    if (toAdd)
                    {
                        var temp = new Interval
                        {
                            Row = c[0],
                            Begin = c[1],
                            End = c[2]
                        };
                        all.Add(temp);
                        list.Add(temp);
                    }
                        

                    all.Sort();

                    for (int j = 1; j < all.Count; ++j)
                    {
                        if (all[j].Begin > all[j-1].End + 1) continue;
                        all[j - 1].End = Math.Max(all[j - 1].End, all[j].End);
                        all.Remove(all[j]);
                        list.Find(e => e == all[j - 1]).End = Math.Max(all[j - 1].End, all[j].End);
                        list.Remove(all[j]);
                        j = 0;
                    }
                }
            }

            BigInteger result = new BigInteger(0);

            foreach (var interval in list)
            {
                result += (interval.End - interval.Begin) + 1;
            }

            Console.WriteLine(new BigInteger(n)* new BigInteger(m) - result);
        }
    }
}
