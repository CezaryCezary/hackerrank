﻿namespace CleanCoder.HackerRank.Contests.WeekOfCode27
{
    public class HackonacciMatrixRotations
    {
        /// <summary>
        /// https://www.hackerrank.com/contests/w27/challenges/hackonacci-matrix-rotations
        /// </summary>
        /// <param name="args"></param>
        public static void Solve(String[] args)
        {
            var tokens_n = InputReader.ReadIntArray();
            int n = tokens_n[0];
            int q = tokens_n[1];

            //YES == X, NO = Y
            bool[,] pattern = new bool[7, 7]{
            {false, true, true, true, true, false, false},      //Y X X X X Y Y
            {true, true, false, false, true, true, false},      //X X Y Y X X Y
            {true, false, true, true, false, true, false},      //X Y X X Y X Y
            {true, false, true, true, false, true, false},      //X Y X X Y X Y
            {true, true, false, false, true, true, false},      //X X Y Y X X Y
            {false, true, true, true, true, false, false},      //Y X X X X Y Y
            {false, false, false, false, false, false, false}   //Y Y Y Y Y Y Y
        };

            bool[,] matrix = new bool[n, n];

            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    matrix[i, j] = pattern[i % 7, j % 7];
                }
            }

            var matrix90RotationResult = Rotate90(matrix, n);
            var matrix180RotationResult = Rotate180(matrix, n);

            while (q-- > 0)
            {
                int angle = Convert.ToInt32(Console.ReadLine()) % 360;
                switch (angle)
                {
                    case 90:
                        Console.WriteLine(matrix90RotationResult);
                        break;
                    case 180:
                        Console.WriteLine(matrix180RotationResult);
                        break;
                    case 270:
                        Console.WriteLine(matrix90RotationResult);
                        break;
                    default:
                        Console.WriteLine(0);
                        break;
                }
            }
        }

        public static int Rotate90(bool[,] matrix, int n)
        {
            int numOfChanges = 0;
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (matrix[i, j] != matrix[n - j - 1, i])
                    {
                        ++numOfChanges;
                    }
                }
            }
            return numOfChanges;
        }

        public static int Rotate180(bool[,] matrix, int n)
        {
            int numOfChanges = 0;
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    if (matrix[i, j] != matrix[n - j - 1, n - i - 1])
                    {
                        ++numOfChanges;
                    }
                }
            }
            return numOfChanges;
        }

        //TODO: Add helper classes to observe above pattern
    }
}
