﻿namespace CleanCoder.HackerRank.Mathematics.Fundamentals
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/is-fibo
    /// Optimizations - generate fibonnaci numbers once and then check if selected number exists in a list
    /// </summary>
    public class IsFibo
    {
        private static List<long> fiboNumbers = new List<long>(50);

        public IsFibo()
        {
            FillFiboTable();
        }

        public string Solve(long n)
        {
            return fiboNumbers.Exists(e => e.Equals(n)) ? "IsFibo" : "IsNotFibo";
        }

        private static void FillFiboTable()
        {
            long max = 10000000001;
            fiboNumbers.Add(0);
            fiboNumbers.Add(1);
            fiboNumbers.Add(2);
            fiboNumbers.Add(3);
            fiboNumbers.Add(5);
            fiboNumbers.Add(8);

            long j = 5, k = 8;
            for (long i = 13; i < max; i=j+k)
            {
                fiboNumbers.Add(i);
                j = k;
                k = i;
            }
        }
    }
}
