﻿namespace CleanCoder.HackerRank.Mathematics.NumberTheory
{
    public static class IdentifySmithNumbers
    {
        /// <summary>
        /// Returns all prime factors of a given number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        /// source for fast execution without Sieve of Eratosthenes: http://www.geeksforgeeks.org/print-all-prime-factors-of-a-given-number/
        public static IList<int> AllPrimeFactorsOfAGivenNumber_FasterWay(int number)
        {
            var primeFactors = new List<int>();

            while (number%2 == 0)
            {
                number /= 2;
                primeFactors.Add(2);
            }

            for (var i = 3; i <= Math.Sqrt(number); i+=2)
            {
                while (number%i == 0)
                {
                    number /= i;
                    primeFactors.Add(i);
                }
            }

            if (number > 2) //condition for prime numbers
            {
                primeFactors.Add(number);
            }

            return primeFactors;
        }

        public static int SumDigits(int number)
        {
            var sum = 0;
            while (number > 0)
            {
                sum += number % 10;
                number /= 10;
            }

            return sum;
        }

        /// <summary>
        /// Calculates if given number is a Smith number.
        /// A Smith number is a composite number, the sum of whose digits is the sum of the digits
        /// of its prime factors obtained as a result of prime factorization (excluding 1).
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        /// Identify Smith Numbers - https://www.hackerrank.com/challenges/identify-smith-numbers
        public static bool SmithNumber(int number)
        {
            var primeFactors = AllPrimeFactorsOfAGivenNumber_FasterWay(number);
            if (primeFactors.Count == 1)
            {
                return false;
            }
            return primeFactors.Sum(SumDigits) == SumDigits(number);
        }
    }
}