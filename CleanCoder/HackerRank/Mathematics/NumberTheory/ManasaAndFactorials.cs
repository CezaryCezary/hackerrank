﻿namespace CleanCoder.HackerRank.Mathematics.NumberTheory
{
    public static class ManasaAndFactorials
    {
        /// <summary>
        /// Returns the smallest number m which contains at least n number of zeros at the end of m!
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        /// Manasa and Factorials - https://www.hackerrank.com/challenges/manasa-and-factorials
        /// Optimizations are -make factorial result modulo 10^6 (prevents arithmetic overflow)
        public static long Solve_TimeLimitExeeded(long n)
        {
            long factorial = 1;
            long zerosCounter = 0;
            long iterator = 2;
            while (true)
            {
                factorial %= 1000000;
                factorial *= iterator;
                while (factorial % 10 == 0)
                {
                    factorial /= 10;
                    ++zerosCounter;
                    if (zerosCounter == n)
                    {
                        return iterator;
                    }
                }
                ++iterator;
            }
        }

        /// <summary>
        /// Returns the smallest number m which contains at least n number of zeros at the end of m!
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        /// Manasa and Factorials - https://www.hackerrank.com/challenges/manasa-and-factorials
        /// Optimizations - binary search for number with using fast way of counting trailing zeroes in factorial of a number
        public static long Solve(long n)
        {
            long low = 0;
            var high = 5 * n;
            while (low <= high)
            {
                var med = (low + high) / 2;
                var zerosCounter = CountTrailingZeroesInFactorialOfANumber(med);
                if (zerosCounter < n)
                {
                    low = med + 1;
                }
                else
                {
                    high = med - 1;
                }
            }
            return low;
        }

        /// <summary>
        /// Count trailing zeroes in factorial of a number
        /// http://www.geeksforgeeks.org/count-trailing-zeroes-factorial-number/
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static long CountTrailingZeroesInFactorialOfANumber(long n)
        {
            long result = 0;
            for (long i = 5; i <= n; i *= 5)
            {
                result += n / i;
            }
            return result;
        }
    }
}
