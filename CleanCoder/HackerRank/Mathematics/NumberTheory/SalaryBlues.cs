﻿namespace CleanCoder.HackerRank.Mathematics.NumberTheory
{
    /// <summary>
    /// Normalize table and add bonus.
    /// Using greatest common divisor with Euclid's algorithm
    /// https://www.hackerrank.com/challenges/salary-blues
    /// </summary>
    public class SalaryBlues
    {
        public static void Solve(int n, int q)
        {
            var numbers = Console.ReadLine().Split(' ');
            var first = Convert.ToInt64(numbers[0]);
            int counter = 0;
            var oldFirst = first;
            for (int i = 1; i < numbers.Length; i++)
            {
                var second = Convert.ToInt64(numbers[i]);
                if (oldFirst.Equals(second))
                {
                    ++counter;
                }
                first = gcd(first, second);
            }

            while (q-- > 0)
            {
                var k = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine(counter.Equals(numbers.Length-1) ? first + k : gcd(first, k));
            }
        }

        /// <summary>
        /// Count greatest common divisor using Euclid's algorithm
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static long gcd(long a, long b)
        {
            if (a < b) return gcd(b, a);

            if (b == 0) return a;

            return gcd(b, a % b);
        }
    }
}
