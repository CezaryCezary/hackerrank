﻿namespace CleanCoder.HackerRank.Mathematics.NumberTheory
{
    /// <summary>
    /// Returns multiple of x which is closest to a^b
    /// </summary>
    /// https://www.hackerrank.com/challenges/closest-number
    /// Optimization is using combination of multiplication and addition to get to closest to a^b in time log(n)
    public class ClosestNumber
    {
        public ClosestNumber()
        {
            var t = Convert.ToInt32(Console.ReadLine());
            while (t-- > 0)
            {
                var temp = InputReader.ReadIntArray();
                var a = temp[0];
                var b = temp[1];
                var x = temp[2];
                Console.WriteLine(Solve(a, b, x));
            }
        }

        public static int Solve(int a, int b, int x)
        {
            var number = (int)Math.Pow(a, b);

            var max = number;
            var closestNumber = 0;
            while (max > x)
            {
                var k = x;
                while (k < max) k *= 2;
                k /= 2;
                closestNumber += k;
                max -= k;
            }

            if (closestNumber + x - number < number - closestNumber)
            {
                closestNumber += x;
            }

            return closestNumber;
        }
    }
}
