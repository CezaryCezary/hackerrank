﻿namespace CleanCoder.HackerRank.Mathematics.NumberTheory
{
    public static class LittlePandaPower
    {
        /// <summary>
        /// Returns the value of a^b mod x.
        /// Parameter b can be negative, so in this case there is need to calculate modular inverse
        /// </summary>
        /// <param name="a"> 1 <= a <= 10^6 </param>
        /// <param name="exp"> 10^-6 <= b <= 10^6 </param>
        /// <param name="mod"> 1 <= x <= 10^6 </param>
        /// <returns></returns>
        /// Little Panda Power - https://www.hackerrank.com/challenges/littlepandapower
        /// Optimization is own implementation of power
        public static long Solve(int a, int exp, int mod)
        {
            if (exp >= 0)
            {
                return Power(a, exp, mod);
            }

            var modularInverse = CalculateModularInverse(a, mod);

            return Power(modularInverse, -exp, mod);
        }

        private static long Power(long a, int exp, int mod)
        {
            long result = 1;

            while (exp != 0)
            {
                if ((exp & 1) == 1)
                {
                    result = result * a % mod;
                }
                a = a * a % mod;
                exp >>= 1;
            }

            return result;
        }

        private static long CalculateModularInverse(int a, int mod)
        {
            long modularInverse = 0;
            for (long i = 1; i < mod; i++)
            {
                if (i * a % mod != 1) continue;
                modularInverse = i;
                break;
            }
            return modularInverse;
        }
    }
}
