﻿using System.Numerics;

namespace CleanCoder.HackerRank.Mathematics.NumberTheory
{
    public static class PowerOfLargeNumbers
    {
        public static long Solve(string b, string expStr)
        {
            return (long)BigInteger.ModPow(BigInteger.Parse(b), BigInteger.Parse(expStr), new BigInteger(1000000007));
        }

        /// <summary>
        /// Solution based on BigInteger's did not help in case of timeouts
        /// </summary>
        /// <param name="b"></param>
        /// <param name="expStr"></param>
        /// <returns></returns>
        public static long Solve_TLE2(string b, string expStr)
        {
            const int mod = 1000000007;

            return BigIntPower(BigInteger.Parse(b) % mod, BigInteger.Parse(expStr), mod);
        }

        private static long BigIntPower(BigInteger b, BigInteger exp, int mod)
        {
            BigInteger result = 1;

            while (exp != 0)
            {
                if ((exp & 1) == 1)
                {
                    result = result * b % mod;
                }
                b = b * b % mod;
                exp >>= 1;
            }

            return (int)result;
        }

        /// <summary>
        /// Algorithm makes first number modulo mod using BigInteger from Numerics library,
        /// then converts exponential to binary format and make power using fast multiplication alg
        /// Gives timeout in 6 of 15 tests
        /// </summary>
        /// <param name="b"></param>
        /// <param name="expStr"></param>
        /// <returns></returns>
        /// Solution - http://www.cquestions.com/2011/07/c-program-to-find-power-of-large-number.html
        public static long Solve_TLE(string b, string expStr)
        {
            const int mod = 1000000007;

            long k;
            if (b.Length > 9)
            {
                var result = BigInteger.Parse(b) % new BigInteger(mod);
                k = Convert.ToInt64(result.ToString());
            }
            else
            {
                k = Convert.ToInt64(b);
            }

            return StringPower(k, ConvertFromBase10ToBase2(expStr), mod);
        }
        
        public static string ConvertFromBase10ToBase2(string base10)
        {
            var result = string.Empty;

            var array = base10.ToCharArray();
            var intArray = new int[array.Length];
            for (var i = 0; i < array.Length; i++)
            {
                intArray[i] = array[i] - '0';
            }

            var trailingZeroes = 0;
            var counter = 0;
            while (trailingZeroes < intArray.Length)
            {
                var reminder = DivideBy2(ref intArray);
                result = result.Insert(0, reminder?"1":"0");
                while (trailingZeroes < intArray.Length && intArray[counter] == 0)
                {
                    ++trailingZeroes;
                    ++counter;
                }
            }

            return result;
        }

        public static bool DivideBy2(ref int[] array)
        {
            bool reminderExists;
            var last = array.Length - 1;

            for (var j = 0; j < last; j++)
            {
                reminderExists = array[j] % 2 == 1;
                if (reminderExists)
                {
                    array[j+1] += 10;
                }
                array[j] = array[j] / 2;
                
            }
            
            reminderExists = array[last] % 2 == 1;
            array[last] = array[last] / 2;
            
            return reminderExists;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="exp">Exp string must be delivered in base 2</param>
        /// <param name="mod"></param>
        /// <returns></returns>
        private static long StringPower(long b, string exp, long mod)
        {
            long result = 1;

            for (var i = exp.Length-1; i >= 0; --i)
            {
                if (exp[i] == '1')
                {
                    result = result*b%mod;
                }
                b = b*b%mod;
            }

            return result;
        }
    }
}
