﻿namespace CleanCoder.HackerRank.Tutorials.CrackingTheCodingInterview
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/ctci-balanced-brackets
    /// </summary>
    public class StacksBalancedBrackets
    {
        public string AreBracketsBalanced(string expression)
        {
            var stack = new Stack<char>();

            foreach (var el in expression)
            {
                switch (el)
                {
                    case '{':
                    case '[':
                    case '(':
                        stack.Push(el);
                        break;
                    case ')':
                        if (stack.Count == 0 || stack.Pop() != '(') return "NO";
                        break;
                    case '}':
                        if (stack.Count == 0 || stack.Pop() != '{') return "NO";
                        break;
                    case ']':
                        if (stack.Count == 0 || stack.Pop() != '[') return "NO";
                        break;
                }
            }

            return stack.Count == 0 ? "YES" : "NO";
        }
    }
}
