﻿namespace CleanCoder.HackerRank.Tutorials.CrackingTheCodingInterview
{
    /// <summary>
    /// https://www.hackerrank.com/challenges/ctci-array-left-rotation
    /// </summary>
    public class ArraysLeftRotation
    {
        public int[] Solve()
        {
            var input = Console.ReadLine().Split(' ');
            var size = Convert.ToInt32(input[0]);
            var leftRotations = Convert.ToInt32(input[1]);
            var inputElements = Array.ConvertAll(Console.ReadLine().Split(' '), int.Parse);

            return LeftRotation(size, leftRotations, inputElements);
        }

        public int[] LeftRotation(int size, int leftRotations, int[] inputElements)
        {
            var array = new int[size];
            for (var i = 0; i < size; i++)
            {
                array[(size + i - leftRotations) % size] = inputElements[i];
            }

            return array;
        }

        public void ArrayLeftRotationInPlace(int size, int leftRotations, int[] inputElements)
        {
            for (int i = 0; i < size; i++)
            {
                Console.WriteLine(inputElements[(leftRotations+i)%size]);
                Console.Write(' ');
            }
        }
    }
}
