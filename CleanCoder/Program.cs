﻿using CleanCoder.HackerRank.Contests.WorldCodeSprint8;

namespace CleanCoder;

public class MyBaseClass { }

public class MyClass : MyBaseClass
{
    ~MyClass()
    {

    }
    //public override void Finalize()
    //{
    //    //implementation
    //}
}
public class Employee
{
    public decimal Bonus { get { return 0m; } }
}
public class Manager : Employee
{
    public new decimal Bonus { get { return 10000m; } }
}
internal static class Program
{
    public static event EventHandler<List<int>>? EventName;

    private static void Main(string[] args)
    {
        //ICountryWriter writer = new CountryWriter();
        //writer.WriteCountry(new Country {Id = 1, Name = "Poland", Capital = "Warsaw"});
        //writer.WriteCountry(new Country {Id = 2, Name = "France", Capital = "Paris"});
        //writer.WriteCountry(new Country {Id = 3, Name = "Uk", Capital = "London"});

        //ICountryReader reader = new CountryReader();
        //var countries = reader.ReadCountries("sample.xml", "Country").ToList();


        RoadsAndLibraries.Solve();
        Console.ReadLine();

        int n = 2;
        int power = (int)Math.Pow(2, 3);

        int xor = 0;
        for (int i = 1; i <= n; ++i)
        {
            xor ^= i;
        }
        Console.WriteLine($"xor:{xor}");

        int counter = 0;
        int preSolution = 1;
        int solution = 1;
        for (int i = n; i > 0; --i)
        {
            preSolution = solution;
            for (int j = i + 1; j < power - n + i; ++j)
            {
                xor = xor ^ (j - 1) ^ j;
                Console.WriteLine($"i:{i}:j:{j}::xor:{xor}");
                if (xor == 0) ++counter;
                else
                {
                    if (preSolution == 1) ++solution;
                    else solution += preSolution - 1;
                }
            }
        }
        Console.WriteLine($"counter::{counter}");
        Console.WriteLine($"solution::{solution}");

        long silnia = 1;
        for (long i = 2; i <= n; ++i)
        {
            silnia *= i;
        }
        Console.WriteLine($"solution*silnia::{solution * silnia}");



        byte b1 = 1;
        byte b2 = 2;
        var sum = b1 + b2;
        //dynamic value = 321;
        //Console.WriteLine(value.Length); RunTimeBinderException

        //Heap, stack, while boxing these are different variables
        IEnumerable<int> values = new List<int>();
        //int value = 23;
        //object o = value;
        //o = 33;
        //Console.WriteLine(value);

        Employee paul = new Manager();
        Console.WriteLine("${0}: {1}", paul.Bonus, paul.GetType().Name);

        char ch = '\x20';
        var ii = ~0;
        Console.WriteLine(ii);
        var arr = new[] { 1, 2 };
        int[] arr2 = { 1, 2 };
        var length = arr.Length;
        var d = new List<int> { 1, 2 };

        Console.ReadKey();
    }

    private static void OnEventName(List<int> e)
    {
        if (EventName != null)
        {
            EventName(null, e);
        }
    }
}
