using System.Collections.Generic;
using CleanCoder.HackerRank.Mathematics.NumberTheory;
using Xunit;

namespace CleanCoderTests.HackerRank.Mathematics.NumberTheory;

public class IdentifySmithNumbersTests
{
    [Fact]
    public void SumDigits()
    {
        Assert.Equal(1, IdentifySmithNumbers.SumDigits(1));
        Assert.Equal(6, IdentifySmithNumbers.SumDigits(123));
        Assert.Equal(42, IdentifySmithNumbers.SumDigits(4937775));
    }

    [Fact]
    public void Get_AllPrimeFactors()
    {
        Assert.Equal(new List<int> { 2, 2, 3 }, IdentifySmithNumbers.AllPrimeFactorsOfAGivenNumber_FasterWay(12));
        Assert.Equal(new List<int> { 3, 3, 5, 7 }, IdentifySmithNumbers.AllPrimeFactorsOfAGivenNumber_FasterWay(315));
        Assert.Equal(new List<int> { 1009 }, IdentifySmithNumbers.AllPrimeFactorsOfAGivenNumber_FasterWay(1009));
    }

    [Fact]
    public void SmithNumbers()
    {
        Assert.True(IdentifySmithNumbers.SmithNumber(378));
        Assert.True(IdentifySmithNumbers.SmithNumber(4937775));
        Assert.False(IdentifySmithNumbers.SmithNumber(4937776));

        var smithNumbers = new List<int>();
        for (var i = 2; i < 130; i++)
        {
            if (IdentifySmithNumbers.SmithNumber(i))
            {
                smithNumbers.Add(i);
            }
        }
        Assert.Equal(new List<int> { 4, 22, 27, 58, 85, 94, 121 }, smithNumbers);
    }
}
