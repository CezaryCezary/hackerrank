using CleanCoder.HackerRank.Mathematics.NumberTheory;
using Shouldly;
using Xunit;

namespace CleanCoderTests.HackerRank.Mathematics.NumberTheory;

public class PowerOfLargeNumbersTests
{
    [Fact]
    public void DivideBy2()
    {
        var number = new[] { 2, 4 };
        Assert.False(PowerOfLargeNumbers.DivideBy2(ref number));
        number[0].ShouldBe(1);
        number[1].ShouldBe(2);

        number = new[] { 2, 5, 8, 1 };
        Assert.True(PowerOfLargeNumbers.DivideBy2(ref number));
        number[0].ShouldBe(1);
        number[1].ShouldBe(2);
        number[2].ShouldBe(9);
        number[3].ShouldBe(0);
    }

    [Fact]
    public void ConvertFromBase10ToBase2()
    {
        Assert.Equal("1000", PowerOfLargeNumbers.ConvertFromBase10ToBase2("8"));
        Assert.Equal("1111011", PowerOfLargeNumbers.ConvertFromBase10ToBase2("123"));
        Assert.Equal("1000100000111011", PowerOfLargeNumbers.ConvertFromBase10ToBase2("34875"));
        Assert.Equal("1000000000000000000000000000000000", PowerOfLargeNumbers.ConvertFromBase10ToBase2("8589934592"));
        Assert.Equal("110110011111001001000111001010110000100101000000001000010101101011010100001010111000111000101110101110011", PowerOfLargeNumbers.ConvertFromBase10ToBase2("34534985349875439875439875349875"));
    }

    [Fact]
    public void Solve_PowerOfLargeNumbers()
    {
        Assert.Equal(9, PowerOfLargeNumbers.Solve("3", "2"));
        Assert.Equal(1024, PowerOfLargeNumbers.Solve("4", "5"));
        Assert.Equal(2401, PowerOfLargeNumbers.Solve("7", "4"));
        Assert.Equal(735851262, PowerOfLargeNumbers.Solve("34534985349875439875439875349875", "93475349759384754395743975349573495"));
        Assert.Equal(985546465, PowerOfLargeNumbers.Solve("34543987529435983745230948023948", "3498573497543987543985743989120393097595572309482304"));
    }
}
