﻿using CleanCoder.HackerRank.Mathematics.NumberTheory;
using Xunit;

namespace CleanCoderTests.HackerRank.Mathematics.NumberTheory;

public class LittlePandaPowerTests
{
    [Fact]
    public void Solve_LittlePandaPower()
    {
        Assert.Equal(1, LittlePandaPower.Solve(1, 2, 3));
        Assert.Equal(1, LittlePandaPower.Solve(3, 4, 2));
        Assert.Equal(4, LittlePandaPower.Solve(4, -1, 5));
        Assert.Equal(11173, LittlePandaPower.Solve(335735, -946018, 123698));
        Assert.Equal(789082, LittlePandaPower.Solve(326879, 55869, 800267));
        Assert.Equal(652304, LittlePandaPower.Solve(202034, -606119, 969615));
    }
}
