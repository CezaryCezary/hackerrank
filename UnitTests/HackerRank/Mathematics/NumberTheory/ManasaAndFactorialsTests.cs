﻿using CleanCoder.HackerRank.Mathematics.NumberTheory;
using Xunit;

namespace CleanCoderTests.HackerRank.Mathematics.NumberTheory;

public class ManasaAndFactorialsTests
{
    [Fact]
    public void CountTrailingZeroesInFactorialOfANumber()
    {
        Assert.Equal(8, ManasaAndFactorials.CountTrailingZeroesInFactorialOfANumber(35));
        Assert.Equal(6, ManasaAndFactorials.CountTrailingZeroesInFactorialOfANumber(25));
        Assert.Equal(211805, ManasaAndFactorials.CountTrailingZeroesInFactorialOfANumber(847235));
        Assert.Equal(4264777785392000, ManasaAndFactorials.CountTrailingZeroesInFactorialOfANumber(17059111141568050));
    }

    [Fact(Skip = "Time Limit Exceeded")]
    public void Solve_ManasaAndFactorials_TLE()
    {
        Assert.Equal(5, ManasaAndFactorials.Solve_TimeLimitExeeded(1));
        Assert.Equal(10, ManasaAndFactorials.Solve_TimeLimitExeeded(2));
        Assert.Equal(15, ManasaAndFactorials.Solve_TimeLimitExeeded(3));
        Assert.Equal(20, ManasaAndFactorials.Solve_TimeLimitExeeded(4));
        Assert.Equal(25, ManasaAndFactorials.Solve_TimeLimitExeeded(5));
        Assert.Equal(25, ManasaAndFactorials.Solve_TimeLimitExeeded(6));
        Assert.Equal(30, ManasaAndFactorials.Solve_TimeLimitExeeded(7));
        Assert.Equal(35, ManasaAndFactorials.Solve_TimeLimitExeeded(8));
        Assert.Equal(847235, ManasaAndFactorials.Solve_TimeLimitExeeded(211805));
        Assert.Equal(3961440, ManasaAndFactorials.Solve_TimeLimitExeeded(990356));
        Assert.Equal(3087310, ManasaAndFactorials.Solve_TimeLimitExeeded(771822));
        Assert.Equal(3033730, ManasaAndFactorials.Solve_TimeLimitExeeded(758427));
        Assert.Equal(2557300, ManasaAndFactorials.Solve_TimeLimitExeeded(639320));
        Assert.Equal(463585, ManasaAndFactorials.Solve_TimeLimitExeeded(115892));
        Assert.Equal(1227835, ManasaAndFactorials.Solve_TimeLimitExeeded(306954));
        Assert.Equal(586625, ManasaAndFactorials.Solve_TimeLimitExeeded(146652));
        Assert.Equal(3819125, ManasaAndFactorials.Solve_TimeLimitExeeded(954775));
        Assert.Equal(3299760, ManasaAndFactorials.Solve_TimeLimitExeeded(824936));
        Assert.Equal(17059111141568050, ManasaAndFactorials.Solve_TimeLimitExeeded(4264777785391999));
    }

    [Fact]
    public void Solve_ManasaAndFactorials()
    {
        Assert.Equal(5, ManasaAndFactorials.Solve(1));
        Assert.Equal(10, ManasaAndFactorials.Solve(2));
        Assert.Equal(15, ManasaAndFactorials.Solve(3));
        Assert.Equal(20, ManasaAndFactorials.Solve(4));
        Assert.Equal(25, ManasaAndFactorials.Solve(5));
        Assert.Equal(25, ManasaAndFactorials.Solve(6));
        Assert.Equal(30, ManasaAndFactorials.Solve(7));
        Assert.Equal(35, ManasaAndFactorials.Solve(8));
        Assert.Equal(847235, ManasaAndFactorials.Solve(211805));
        Assert.Equal(3961440, ManasaAndFactorials.Solve(990356));
        Assert.Equal(3087310, ManasaAndFactorials.Solve(771822));
        Assert.Equal(3033730, ManasaAndFactorials.Solve(758427));
        Assert.Equal(2557300, ManasaAndFactorials.Solve(639320));
        Assert.Equal(463585, ManasaAndFactorials.Solve(115892));
        Assert.Equal(1227835, ManasaAndFactorials.Solve(306954));
        Assert.Equal(586625, ManasaAndFactorials.Solve(146652));
        Assert.Equal(3819125, ManasaAndFactorials.Solve(954775));
        Assert.Equal(3299760, ManasaAndFactorials.Solve(824936));
        Assert.Equal(17059111141568050, ManasaAndFactorials.Solve(4264777785391999));
    }
}
