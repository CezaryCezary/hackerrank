﻿using CleanCoder.HackerRank.Mathematics.NumberTheory;
using Xunit;

namespace CleanCoderTests.HackerRank.Mathematics.NumberTheory;

public class ClosestNumberTests
{
    [Fact]
    public void Solve_ClosestNumber()
    {
        Assert.Equal(348, ClosestNumber.Solve(349, 1, 4));
        Assert.Equal(392, ClosestNumber.Solve(395, 1, 7));
        Assert.Equal(0, ClosestNumber.Solve(4, -2, 2));
    }
}
