using CleanCoder.HackerRank.Mathematics.Fundamentals;
using Xunit;

namespace CleanCoderTests.HackerRank.Mathematics.Fundamentals;

public class IsFIboTests
{
    [Fact]
    public void IsFibonnacci()
    {
        var fibo = new IsFibo();

        Assert.Equal("IsFibo", fibo.Solve(13));
        Assert.Equal("IsNotFibo", fibo.Solve(123));
        Assert.Equal("IsNotFibo", fibo.Solve(123984376));
    }
}
