﻿using CleanCoder.HackerRank.Contests.WeekOfCode28;
using Xunit;

namespace CleanCoderTests.HackerRank.Contests.WeekOfCode28;

public class TheValueOfFriendshipTests
{
    [Fact]
    public void TheValueOfFriendshipTest()
    {
        var a = new TheValueOfFriendship.Node[4];

        a[0] = new TheValueOfFriendship.Node();
        a[1] = new TheValueOfFriendship.Node { Nodes = { 2, 3 } };
        a[2] = new TheValueOfFriendship.Node { Nodes = { 1 } };
        a[3] = new TheValueOfFriendship.Node { Nodes = { 1 } };

        var counter = TheValueOfFriendship.Count(a);
        Assert.Equal(8, counter);
    }

    [Fact]
    public void TheValueOfFriendshipTest1()
    {
        /* console input
        1
        14 14
        7 8
        1 2
        2 3
        1 3
        4 5
        4 6
        5 6
        10 11
        13 12
        12 11
        13 11
        13 10
        12 10
        12 9*/
        var a = new TheValueOfFriendship.Node[14];

        a[0] = new TheValueOfFriendship.Node();
        a[7] = new TheValueOfFriendship.Node { Nodes = { 8 } };
        a[8] = new TheValueOfFriendship.Node { Nodes = { 7 } };
        a[1] = new TheValueOfFriendship.Node { Nodes = { 2, 3 } };
        a[2] = new TheValueOfFriendship.Node { Nodes = { 1, 3 } };
        a[3] = new TheValueOfFriendship.Node { Nodes = { 1, 2 } };
        a[4] = new TheValueOfFriendship.Node { Nodes = { 5, 6 } };
        a[5] = new TheValueOfFriendship.Node { Nodes = { 4, 6 } };
        a[6] = new TheValueOfFriendship.Node { Nodes = { 4, 5 } };
        a[9] = new TheValueOfFriendship.Node { Nodes = { 12 } };
        a[10] = new TheValueOfFriendship.Node { Nodes = { 11, 12, 13 } };
        a[11] = new TheValueOfFriendship.Node { Nodes = { 10, 12, 13 } };
        a[12] = new TheValueOfFriendship.Node { Nodes = { 9, 10, 11, 13 } };
        a[13] = new TheValueOfFriendship.Node { Nodes = { 10, 11, 12 } };

        var counter = TheValueOfFriendship.Count(a);
        Assert.Equal(352, counter);
    }
}
