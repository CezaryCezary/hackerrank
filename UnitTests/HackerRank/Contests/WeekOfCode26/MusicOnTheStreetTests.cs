﻿using CleanCoder.HackerRank.Contests.WeekOfCode26;
using Xunit;

namespace CleanCoderTests.HackerRank.Contests.WeekOfCode26;

public class MusicOnTheStreetTests
{
    [Fact]
    public void Solve_MusicOnTheStreet()
    {
        Assert.Equal(-1, MusicOnTheStreet.SolveTrialAndErrorMethod(2, new[] { 1, 3 }, 7, 2, 3));
        Assert.Equal(-3, MusicOnTheStreet.SolveTrialAndErrorMethod(2, new[] { 1, 4 }, 4, 3, 6));
        Assert.Equal(2, MusicOnTheStreet.SolveTrialAndErrorMethod(4, new[] { 1, 4, 6, 8 }, 8, 2, 2));
        Assert.Equal(-2, MusicOnTheStreet.SolveTrialAndErrorMethod(5, new[] { 1, 3, 5, 9, 19 }, 11, 2, 5));
        Assert.Equal(6, MusicOnTheStreet.SolveTrialAndErrorMethod(5, new[] { 1, 7, 8, 9, 12 }, 3, 1, 1));
        Assert.Equal(5, MusicOnTheStreet.SolveTrialAndErrorMethod(5, new[] { 1, 7, 10, 13, 15 }, 10, 2, 3));
        Assert.Equal(-1, MusicOnTheStreet.SolveTrialAndErrorMethod(3, new[] { 1, 7, 8 }, 5, 1, 3));
        Assert.Equal(0, MusicOnTheStreet.SolveTrialAndErrorMethod(3, new[] { 1, 7, 15 }, 13, 3, 6));
        Assert.Equal(10, MusicOnTheStreet.SolveTrialAndErrorMethod(10, new[] { 1, 2, 3, 5, 7, 8, 9, 12, 15, 18 }, 5, 2, 4));
    }
}
