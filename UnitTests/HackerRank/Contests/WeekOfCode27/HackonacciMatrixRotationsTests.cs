﻿using CleanCoder.HackerRank.Contests.WeekOfCode27;
using System;
using Xunit;

namespace CleanCoderTests.HackerRank.Contests.WeekOfCode27;

public class HackonacciMatrixRotationsTests
{
    [Fact]
    public void SolveHackonacciMatrixRotations()
    {
        int n = 21;
        var matrix = HackonacciMatrixRotations1(n);
        string str = string.Empty;
        for (int i = 0; i < n; ++i)
        {
            for (int j = 0; j < n; ++j)
            {
                str += matrix[i, j] ? "X " : "Y ";
            }
            str += Environment.NewLine;
        }

    }

    private static int MOD = 100;

    static bool[,] HackonacciMatrixRotations1(int n)
    {
        int[,] F = new int[3, 3];

        bool[,] matrix = new bool[n, n];

        for (int i = 1; i <= n; ++i)
        {
            for (int j = i; j <= n; ++j)
            {
                F = new int[3, 3] { { 1, 2, 3 }, { 1, 0, 0 }, { 0, 1, 0 } };
                long t = i * i * j * j;
                if (t == 1) matrix[i - 1, j - 1] = false;
                else matrix[i - 1, j - 1] = Power(F, t - 2) % 2 == 0;//YES == X, NO = Y
                matrix[j - 1, i - 1] = matrix[i - 1, j - 1];
            }
        }

        int[] result = new int[3];
        result[0] = HackonacciMatrixRotations.Rotate90(matrix, n);
        result[1] = HackonacciMatrixRotations.Rotate180(matrix, n);

        //Rotation 270
        result[2] = HackonacciMatrixRotations.Rotate90(matrix, n);

        return matrix;
    }

    static int Power(int[,] F, long n)
    {
        int[,] M = new int[3, 3] { { 1, 2, 3 }, { 1, 0, 0 }, { 0, 1, 0 } };

        if (n == 1) return F[0, 0] + F[0, 1] + F[1, 1];

        Power(F, n / 2);

        Multiply(F, F);

        if (n % 2 == 1) Multiply(F, M);

        return F[0, 0] + F[0, 1] + F[1, 1];
    }

    static void Multiply(int[,] F, int[,] M)
    {
        int[,] mul = new int[3, 3];

        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                for (int k = 0; k < 3; ++k)
                {
                    mul[i, j] += F[i, k] * M[k, j];
                }
            }
        }

        for (int i = 0; i < 3; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                F[i, j] = mul[i, j] % MOD;
            }
        }
    }
}
