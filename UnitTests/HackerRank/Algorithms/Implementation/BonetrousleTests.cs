using System.Collections.Generic;
using System.Linq;
using CleanCoder.HackerRank.Algorithms.Implementation;
using Xunit;

namespace CleanCoderTests.HackerRank.Algorithms.Implementation;

public class BonetrousleTests
{
    [Fact]
    public void SolveBonetrousle()
    {
        Assert.Equal(new List<ulong> { 3, 4, 5 }, Bonetrousle.Solve(12, 8, 3));

        Assert.Equal(new List<ulong> { 2 }, Bonetrousle.Solve(2, 3, 1));
        Assert.Equal(new List<ulong> { 3 }, Bonetrousle.Solve(3, 3, 1));
        Assert.Equal(new List<ulong>(), Bonetrousle.Solve(172, 17, 9));
        Assert.Equal(new List<ulong> { 2, 3, 4, 5, 7, 8, 9 }, Bonetrousle.Solve(38, 10, 7));

        var result = Bonetrousle.Solve(125, 16, 14);
        Assert.Equal(125UL, result.Aggregate(0UL, (a, c) => a + c));
    }
}
