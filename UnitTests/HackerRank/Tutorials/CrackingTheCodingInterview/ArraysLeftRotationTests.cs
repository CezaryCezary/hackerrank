﻿using CleanCoder.HackerRank.Tutorials.CrackingTheCodingInterview;
using System.Linq;
using Xunit;

namespace CleanCoderTests.HackerRank.Tutorials.CrackingTheCodingInterview;

public class ArraysLeftRotationTests
{
    [Fact]
    public void LeftRotation()
    {
        var sut = new ArraysLeftRotation();
        var expResult = new[] { 5, 1, 2, 3, 4 };
        var result = sut.LeftRotation(5, 4, new[] { 1, 2, 3, 4, 5 });

        Assert.True(result.SequenceEqual(expResult));
    }
}
