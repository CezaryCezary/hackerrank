﻿using CleanCoder.HackerRank.Tutorials.CrackingTheCodingInterview;
using Xunit;

namespace CleanCoderTests.HackerRank.Tutorials.CrackingTheCodingInterview;

public class StacksBalancedBracketsTests
{
    [Fact]
    public void SampleTestCases()
    {
        var sut = new StacksBalancedBrackets();
        Assert.Equal("YES", sut.AreBracketsBalanced("{{[[(())]]}}"));
        Assert.Equal("NO", sut.AreBracketsBalanced("{[(])}"));
        Assert.Equal("NO", sut.AreBracketsBalanced("{{}("));
        Assert.Equal("NO", sut.AreBracketsBalanced("{[](}([)(])[]]})()]){[({]}{{{)({}(][{{[}}(]{"));
        Assert.Equal("NO", sut.AreBracketsBalanced(")})[(]{][[())]{[]{{}}[)[)}[]){}](}({](}}}[}{({()]]"));
        Assert.Equal("YES", sut.AreBracketsBalanced("((()))[]{[(()({[()({[]}{})]}))]}{[]}{{({}{})[{}{}]{()([()])[{()}()[[]{}()]{}{}[]()]}[[]{[]}([])]}}"));
    }
}
