﻿using CleanCoder;
using System.Collections.Generic;
using Xunit;

namespace CleanCoderTests;

public class AlgorithmsTests
{
    [Fact]
    public void Factorial()
    {
        Assert.Equal(1, Algorithms.CalculateFactorial(1));
        Assert.Equal(2, Algorithms.CalculateFactorial(2));
        Assert.Equal(2432902008176640000, Algorithms.CalculateFactorial(20));
    }

    [Fact]
    public void Factorial_TooBigNumber_IsWrong()
    {
        Assert.Equal(-4249290049419214848, Algorithms.CalculateFactorial(21));
    }

    [Fact]
    public void SieveOfEratosthenes_UpTo10()
    {
        var result = Algorithms.SieveOfEratosthenes(10);
        Assert.Equal(new List<int> { 2, 3, 5, 7 }, result);
    }

    [Fact]
    public void SieveOfEratosthenes_UpTo100()
    {
        var result = Algorithms.SieveOfEratosthenes(100);
        Assert.Equal(new List<int> { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 }, result);
    }

    [Fact]
    public void SieveOfEratosthenes_From1000_To1050()
    {
        var result = Algorithms.SieveOfEratosthenes(1000, 1050);
        Assert.Equal(new List<int> { 1009, 1013, 1019, 1021, 1031, 1033, 1039, 1049 }, result);
    }

    [Fact]
    public void AllPrimeFactors()
    {
        Assert.Equal(new List<int> { 2, 2, 3 }, Algorithms.AllPrimeFactorsOfAGivenNumber(12));
        Assert.Equal(new List<int> { 3, 3, 5, 7 }, Algorithms.AllPrimeFactorsOfAGivenNumber(315));
        Assert.Equal(new List<int> { 1009 }, Algorithms.AllPrimeFactorsOfAGivenNumber(1009));
    }
}
