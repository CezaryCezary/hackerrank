﻿using CleanCoder;
using System.Collections.Generic;
using Xunit;

namespace CleanCoderTests;

public class GraphTests
{
    [Fact]
    public void BfsImplementation()
    {
        //create adjacency list
        var a = new Node[10];

        a[0] = new Node { Nodes = { 1, 2 } };
        a[1] = new Node { Nodes = { 2, 0 } };
        a[2] = new Node { Nodes = { 0, 1 } };

        var sut = new Graph();
        var result = sut.BreadthFirstSearch(a);
        Assert.Equal(new List<int> { 0, 1, 2 }, result);
    }
}
